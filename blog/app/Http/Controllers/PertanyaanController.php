<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PertanyaanController extends Controller
{
    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('listpertanyaan', compact('pertanyaan'));
    }
    public function create(){
        return view('pertanyaan');
    }
    public function store(Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);
        $query = DB::table('pertanyaan')->insert([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'tanggal_dibuat' => date('Y-m-d'),
            'tanggal_diperbaharui' => date('Y-m-d'),
        ]);
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Dibuat!');
    }
    public function show($id){
        $pertanyaan = DB::table('pertanyaan')->where('id',$id)->first();
        return view('post.show', compact('pertanyaan'));
    }
    public function edit($id){
        $pertanyaan = DB::table('pertanyaan')->where('id',$id)->first();
        return view('post.edit', compact('pertanyaan'));
    }
    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);
        $query = DB::table('pertanyaan')->where('id',$id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'tanggal_diperbaharui' => date('Y-m-d'),
        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Di Update!');
    }
    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id',$id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Di Hapus!');
    }
}

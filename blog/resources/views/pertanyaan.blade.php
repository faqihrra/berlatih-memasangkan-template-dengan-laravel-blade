@extends('layoutadminlte.master')

@section('content-header')
<div class="row mb-2">
  <div class="col-sm-6">
    <h1>Pertanyaan</h1>
  </div>
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="/pertanyaan">Pertanyaan</a></li>
      <li class="breadcrumb-item active">Show</li>
    </ol>
  </div>
</div>
@endsection

@section('content')
@error('judul')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
@enderror
@error('isi')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
@enderror
<div class="card">
    <div class="card-body">
        <form method="POST" class="form-group" action="/pertanyaanpost">
          @csrf
            <label for="pertanyaan">Buat Pertanyaan</label>
            <input type="text" id="pertanyaan" name="judul" class="form-control" placeholder="Judul Pertanyaan...">
            <label for="my-textarea">Isi Pertanyaan</label>
            <textarea id="my-textarea" class="form-control" style="resize: none;" name="isi" rows="4" placeholder="Isi Pertanyaan..."></textarea>
            <hr>
            <div class="d-flex justify-content-end">
              <input class="btn btn-primary" type="submit" name="submit" value="Submit">
            </div>
        </form>
    </div>
</div>
@error('body')
  <div class="alert alert-danger">
    {{ $message }}
  </div>
@enderror
@endsection
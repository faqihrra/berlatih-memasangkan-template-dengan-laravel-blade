<!DOCTYPE html>
<html>
@include('layoutadminlte.partial.header')
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  @include('layoutadminlte.partial.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('layoutadminlte.partial.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        @yield('content-header')
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @yield('content')
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('layoutadminlte.partial.footer')

  <!-- Control Sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
@include('layoutadminlte.partial.js')
</body>
</html>

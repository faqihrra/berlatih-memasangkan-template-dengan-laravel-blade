@extends('layoutadminlte.master')

@section('content-header')
<div class="row mb-2">
  <div class="col-sm-6">
    <h1>Edit Pertanyaan Dengan Id : {{$pertanyaan->id}}</h1>
  </div>
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="/pertanyaan">Pertanyaan</a></li>
      <li class="breadcrumb-item active">Edit</li>
    </ol>
  </div>
</div>
@endsection

@section('content')
@error('judul')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
@enderror
@error('isi')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
@enderror
<div class="card">
    <div class="card-body">
        <form method="POST" class="form-group" action="/pertanyaan/{{$pertanyaan->id}}">
        @method('PUT')
          @csrf
            <label for="pertanyaan">Judul Pertanyaan</label>
            <input type="text" id="pertanyaan" name="judul" class="form-control" value="{{$pertanyaan->judul}}">
            <label for="my-textarea">Isi Pertanyaan</label>
            <textarea id="my-textarea" class="form-control" style="resize: none;" name="isi" rows="4">{{$pertanyaan->isi}}</textarea>
            <hr>
            <div class="d-flex justify-content-end">
              <input type="submit" name="submit" class="btn btn-primary" value="Update">
            </div>
        </form>
    </div>
</div>
@endsection
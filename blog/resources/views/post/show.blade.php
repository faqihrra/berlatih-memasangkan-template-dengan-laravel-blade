@extends('layoutadminlte.master')

@section('content-header')
<div class="row mb-2">
  <div class="col-sm-6">
    <h1>Show Pertanyaan Dengan Id : {{$pertanyaan->id}}</h1>
  </div>
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="/pertanyaan">Pertanyaan</a></li>
      <li class="breadcrumb-item active">Show</li>
    </ol>
  </div>
</div>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <form class="form-group" action="">
          @csrf
            <label for="pertanyaan">Judul Pertanyaan</label>
            <input readonly="" type="text" id="pertanyaan" name="judul" class="form-control" value="{{$pertanyaan->judul}}">
            <label for="my-textarea">Isi Pertanyaan</label>
            <textarea readonly="" id="my-textarea" class="form-control" style="resize: none;" name="isi" rows="4">{{$pertanyaan->isi}}</textarea>
            <hr>
            <div class="d-flex justify-content-end">
              <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-primary">Edit Pertanyaan</a>
            </div>
        </form>
    </div>
</div>
@endsection